#ifndef WARSTWA_H
#define WARSTWA_H

#include <QVector>
#include "neuron.h"

class Warstwa
{
public:
    Warstwa();
    Warstwa(int i, int iloscWejsc);

    QVector<Neuron> warstwa;
    int ilosc;

    void setWarstwa(QVector<Neuron> warstwa);
    QVector<Neuron> getWarstwa();

    void dodaj(Neuron n);
    Neuron zwroc(int i);
    void zamien(Neuron n, int i);
    void operacjeNaWarstwie(Warstwa wczesniejsza);
    void operacjeWstecz(Warstwa nastepna);
    void operacjeWsteczKoncowaWarstwa(double x, double y);

    void uaktualnijWagi(Warstwa wczesniejsza);
    void uaktualnijWagi(double x1, double x2);

    double sigma(double s);
};

#endif // WARSTWA_H
