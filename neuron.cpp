/**
 * 28 stycznia 2019
 * RafKac
 *
 * klasa odpowiada za pojedynczy neuron.
 * Jako parametr wywołania otrzymuje ilość wejść.
 * Zwraca kąt.
 *
 * W ostatniej warstwie: pierwszy zwraca kąt aplha, drugi zwraca kąt beta.
 *
 *
 * 7 lutego 2019
 *
 * UWAGA - przyjmujemy konwencję, że waga dla pierwszego wejścia to waga dla jedynki. Czyli tablicaWag[0] to w0.
 *
 */
#include "neuron.h"
#include "warstwa.h"
#include <math.h>

Neuron::Neuron(){
    wejscia = 3;          // ilość danych wejściowych
    tablicaWag = QVector<double>();
    for(int i = 0; i < wejscia; i++){
        double waga = 2.0 * ((double)rand()/ RAND_MAX) - 1;
        tablicaWag.push_back(waga);
    }
    stalaUczenia = 0.001;
    delta = 0;
    sumaWejsciowa = 0;
    wynik = 0;
    prog = 1;
}

Neuron::Neuron(int k){
    tablicaWag = QVector<double>();
    for(int i = 0; i < k; i++){
        double waga = 2.0 * ((double)rand()/ RAND_MAX) - 1;
        tablicaWag.push_back(waga);
    }
    wejscia = k;          // ilość danych wejściowych
    stalaUczenia = 0.001;
    delta = 0;
    sumaWejsciowa = 0;
    wynik = 0;
    prog = 1;
}

void Neuron::setWejscia(int wejscia){
    this->wejscia = wejscia;
}

int Neuron::getWejscia(){
    return wejscia;
}

void Neuron::setWynik(double wynik){
    this->wynik = wynik;
}

double Neuron::getWynik(){
    return wynik;
}

void Neuron::setTablicaWag(QVector<double> tablicaWag){
    this->tablicaWag = tablicaWag;
}

QVector<double> Neuron::getTablicaWag(){
    return tablicaWag;
}

void Neuron::setStalaUczenia(double stalaUczenia){
    this->stalaUczenia = stalaUczenia;
}

double Neuron::getStalaUczenia(){
    return stalaUczenia;
}

void Neuron::setDelta(double delta){
    this->delta = delta;
}

double Neuron::getDelta(){
    return delta;
}

/**
 * 14 lutego 2019
 * Metoda dostaje jako parametry indeks wagi do zmiany i wartość,
 * jaka po zmianie ma tam być.
 */
double Neuron::zmienWage(int i, double wartosc){
    tablicaWag[i] = wartosc;
}

double Neuron::getSumaWejsciowa(){
    return sumaWejsciowa;
}

void Neuron::setSumeWejsciowa(double sumaWejsciowa){
    this->sumaWejsciowa = sumaWejsciowa;
}

/**
 * 28 stycznia 2019
 * Metoda odpowiada za uaktualnienie wag, które w każdym neuronie zachodzi tak samo.
 *
 * Wzór:
 * w_{k,j}^(m+1) = w_{k,j}^(m) - stalaUczenia * delta_k * y_j;      - wyjsciowa
 * w_{j,i}^(m+1) = w_{j,i}^(m) - stalaUczenia * delta_j * x_i;      - ukryte
 *
 * Zatem jako parametr wywołania dostaje wartość przykładu, na którym operujemy - (1, x, y).
 *
 * 13 lutego 2019
 * Tutaj trzeba zmodyfikować paramter wywołania, bo korzystamy z wszystkich wartości, jakie wchodzą do tego
 * neurona.
 */
void Neuron::uaktualnijWagi(double w){
    double p = 0;
    for(int i = 0 ; i < tablicaWag.size(); i++){
        p = tablicaWag.at(i);
        p = p - stalaUczenia * delta * w;
    }
}

/**
 * 14 lutego 2019
 * Metoda służy uaktualnieniu wag w pojedynczym neuronie.
 * Na wejściu dostaje całą wcześniejszą warstwę.
 * Na jej podstawie uaktualnia wagi w danym neuronie.
 *
 * Najpierw uaktualniamy wagę zerową (tą dla progu),
 * później kolejne.
 */
void Neuron::uaktualnijWagi(Warstwa wczesniejsza){
    qDebug("UaktualnijWagi(Warstwa) - dla neurona");
    double waga = tablicaWag.at(0);
    waga = waga - stalaUczenia * getDelta() * wczesniejsza.getWarstwa().at(0).getWynik();
    zmienWage(0, waga);
    waga = 0;
    for(int numerWagi = 1; numerWagi < tablicaWag.size(); numerWagi++){
        waga = tablicaWag.at(numerWagi);
        waga = waga - stalaUczenia * getDelta() * wczesniejsza.getWarstwa().at(numerWagi).getWynik();
        zmienWage(numerWagi, waga);
    }
    qDebug("UaktualnijWagi(Warstwa) - dla neurona - koniec");
}


/**
 * 9 lutego 2019
 * Metoda liczy sumę wejściową. Ponadto liczy wartość funkcji aktywacji na sumie wejściowej.
 * Wszystko wyliczone ustawia w danym neuronie.
 */
void Neuron::liczSumeWejsciowa(QVector<double> wejscie){
    double suma = tablicaWag.at(0);
    qDebug("Dane: ilość Wag: %d, ilość wejść: %d, suma: %f", tablicaWag.size(), wejscie.size(), suma);
    for(int licznik = 1; licznik < tablicaWag.size(); licznik++){
        qDebug("%d", licznik);
        suma = suma + tablicaWag.at(licznik) * wejscie.at(licznik-1);
    }
    sumaWejsciowa = suma;
    wynik = sigma(sumaWejsciowa);
    qDebug("LiczSumeWejscia.koniec");
}


/**
 * 28 stycznia 2019
 * Metoda liczy wartośc funkcji sigma dla podanego argumentu.
 */
double Neuron::sigma(double s){
    double w = 0.0;
    w = 1.0 / (1.0 + pow(M_E, -s));
    return w;
}

