#-------------------------------------------------
#
# Project created by QtCreator 2019-01-28T09:25:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Robot
TEMPLATE = app


SOURCES += main.cpp\
        robot.cpp \
    neuron.cpp \
    warstwa.cpp

HEADERS  += robot.h \
    neuron.h \
    warstwa.h

FORMS    += robot.ui
