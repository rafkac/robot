#include "robot.h"
#include "ui_robot.h"
#include <math.h>
#include <QLine>
#include <QPainter>
#include <QColor>
#include <QRgb>

/**
 * 28 stycznia 2019
 *
 * Program realizuje zadanie 4, czyli ramię robota.
 * Problem:
 *  czy kliknięty punkt jest osiągalny dla robota z ramionami o ustalonej długości?
 *  Jeśli tak, to jakie kąty muszą być między ramionami?
 *
 * Będziemy wykorzystywać neurony (dość podobne do perceptronów), gdzie ilość wejść będziemy definiowali
 * jako parametr wywołania.
 * Tak samo ilość neuronów w warstwie oraz ilość warstw.
 * Pierwsza warstwa ma tylko trzy wejścia: 1, x, y.
 * Druga warstwa na wejściu przyjmuje 1 oraz wyjścia z pierwszej warstwy,
 * trzecia analogicznie.
 *
 * Ponadto będziemy rysowali na drugim rysunku punky, które trafiliśmy podczas uczenia
 * (konkretnie będziemy rozjaśniali piksel w danym miejscu).
 *
 * Uczenie - będziemy losowali kąty i liczyli, gdzie możemy sięgnąć.
 *
 * Wyjsciowe kąty alpha, beta należą do przedziałów (0, pi).
 * Implementujemy algorytm wstecznej propagacji błędów.
 *
 * Algorytm wstecznej propagacji błędu:
 * 1) wybieramy małe wagi początkowe. Wybieramy niewielką stałą uczenia > 0.
 * 2) iterujemy, póki błąd ERROR się zmniejsza (ewentualnie duże odchylenie dla pojedyńczych przykładów
 * należy traktować jako przejaw zaszumienia danych wejściowych niż niedoskonałości sieci)
 *      2.1) losujemy przykład x z wyjsciem t,
 *      2.2) przebiegamy sieć w przód, dla każdej jednostki zapamiętując sumę wejściową i jej wyjscie
 *              (wartość funkcji aktywacji na sumie wejściowej)
 *      2.3) przebiegamy sieć wstecz, liczymy delta dla kazdej jednostki wyjściowej, a następnie ukrytych.
 *      2.4) zmieniamy wagi.
 *
 *
 * Nasza klasa musi mieć metoda dające funkcjonalności:
 *  - liczenia błędu,
 *  - obsługi kliknieć,
 *  - rysowania ramion robota,
 *  - uczenia sieci,
 *  - zaznaczania nauczonego punktu na drugim rysunku,
 *  - liczenie delty (zależne od warstwy)
 *
 * UWAGA:
 * 1. linie będziemy rysowali za pomocą komendy QLine(x1, y1, x2, y2);
 * 2. Przykłady muszą być normalizowane.
 */


Robot::Robot(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Robot)
{
    ui->setupUi(this);
    srand(time(NULL));
    img1 = new QImage(300, 300, QImage::Format_RGB32);
    img2 = new QImage(300, 300, QImage::Format_RGB32);
    img1->fill(Qt::white);
    img2->fill(Qt::black);

    dlugoscRamienia = 50;
    iloscWarstwUkrytych = 1;
    warstwaWyj = QVector<Neuron>();
    warstwyUkryte = QVector<Warstwa>();

    czyJestTrybNauki = true;

    connect(ui->przyciskExit, SIGNAL(clicked()), this, SLOT(close()) );
    connect(ui->przyciskUcz, SIGNAL(clicked()), this, SLOT(uczSiec()) );
    connect(ui->przyciskCzysc, SIGNAL(clicked()), this, SLOT(czyscEkran()) );
    x = 0;
    y = 0;

    centralny = QPoint(100, 150);
    img1->setPixel(centralny, Qt::red);
    img1->setPixel(100, 151, Qt::red);
    img1->setPixel(100, 149, Qt::red);
    img1->setPixel(101, 150, Qt::red);
    img1->setPixel(99, 150, Qt::red);
    img2->setPixel(100, 150, Qt::yellow);
    img2->setPixel(100, 149, Qt::yellow);
    img2->setPixel(100, 151, Qt::yellow);
    img2->setPixel(99, 150, Qt::yellow);
    img2->setPixel(101, 150, Qt::yellow);

    // przyjmujemy, że mamy dwa neurony w warstwie ukrytej
    for(int j = 0; j < 2; j++){
        Neuron n = Neuron(3);
        warstwaWyj.push_back(n);
    }

    // teraz inicjujemy warstwy ukryte, konkretnie - dwie w tym przypadku, w każdej warstwie są dwa neurony,
    // dlatego każdy neuron ma trzy wejścia
    for(int j = 0; j < 2; j++){
        Warstwa w = Warstwa(2,3);
        warstwyUkryte.push_back(w);
    }

}

Robot::~Robot()
{
    delete ui;
}


/**
 * 28 stycznia 2019
 * Metoda liczy wartość funkcji sigma dla podanego argumentu.
 */
double Robot::sigma(double s){
    double w = 0;
    w = 1.0 / (1.0 + pow(M_E, -s));
    return w;
}

/**
 * 28 stycznia 2019
 * w tej metodzie losujemy liczę z przedziału (0,1) i mnożymy razy Pi,
 * co nam da kąt z przedziału (0, 180) stopni.
 * Jeśli nie przemnożymy przez Pi, to będziemy mieć wartość konta znormalizowaną.
 */
double Robot::losujKat(){
    double waga = (double)rand()/ RAND_MAX;
    return waga;
}

/**
 * 28 stycznia 2019
 * Metoda służy do sprawdzenia, czy kliknięty kąt jest osiągalny przez ramię robota.
 * Jeśli jest, to rysujemy ramię.
 * Jeśli nie jest, to mamy problem.
 */
void Robot::sprawdzamy(){
//    QPoint t1 = QPoint(x, y);
//    rysujLinie(centralny, t1);
    rysujPunkt(x, y);

}

/**
 * 28 stycznia 2019
 * Metoda bierze jako parametry wywołania współrzędne punktu (np podczas uczenia)
 * a następnie wczytuje kolor punktu i nieco go przyciemnia.
 * kolorujemy img2, więc współrzędne muszą być przesunięte w prawo o 310 px.
 *
 * Przy czym nie musimy dodawać przesunięcia, bo podajemy bezpośrednie współrzędne miejsca,
 * a przesunięcie następuje w skutek podpięcia img2.
 *
 */
void Robot::kolorujPunkt(int x1, int y1){
//    qDebug("kolorujPunkt(%d, %d) - początek", x1, y1);
    int xW = x1 ;//+ 310;
    int yW = y1;
    int r = 0;
    int g = 0;
    int b = 0;
    QRgb kolor;
    QColor col;
    if(xW < img2->width() && xW > 0 && yW < img2->height() && yW > 0){
        kolor = img2->pixel(xW, yW);
        col = kolor;
        r = col.red();
        g = col.green();
        b = col.blue();
        qDebug("Kolor: (%d, %d, %d)", r, g, b);
        if(r < 250){
            r = r + 5;
        }
        if(g < 250){
            g = g + 5;
        }
        if(b < 250){
            b = b + 5;
        }
        kolor = qRgb(r, g, b);
    }
    img2->setPixel(xW, yW, kolor);
    update();
//    qDebug("kolorujPunkt(int, int) - koniec");
}

/**
 * 9 lutego 2019
 * Metoda odpowiada za zamalowanie punktu w img1.
 * Zamalowujemy także jego otocznie o promieniu 1, aby punkt był widoczny.
 */
void Robot::rysujPunkt(int xP, int yP){
//    qDebug("rysujPunkt(%d, %d)",xP, yP);
    img1->setPixel(xP, yP, Qt::red);
    img1->setPixel(xP + 1, yP, Qt::red);
    img1->setPixel(xP - 1, yP, Qt::red);
    img1->setPixel(xP, yP + 1, Qt::red);
    img1->setPixel(xP, yP - 1, Qt::red);
    update();
}


/**
 * 28 stycznia 2019
 * Poniższe dwie metody służą do zaznaczania punktów na rysunkach
 * oraz do rysowania obrazków.
 */
void Robot::paintEvent(QPaintEvent *){
    QPainter p(this);
    p.drawImage(0, 0, *img1);
    p.drawImage(310, 0, *img2);
}

void Robot::mousePressEvent(QMouseEvent *event){
    if(czyJestTrybNauki == false){
        x = event->x();
        y = event->y();
        sprawdzamy();
    }
    else {
        qDebug("Jest tryb nauki");
    }
}

/**
 * 9 lutego 2019
 *
 * Metoda przyjmuje jako parametry wywołania wartości dwóch kątów.
 * Zwraca punkt, który zostanie osiągnięty przy takich kątach.
 *
 * Zatem najpierw liczymy punkt złamania ręki (kąt złamania już mamy),
 * a następnie wyliczamy punkt końcowy (druga część ramienia odchodzi pod kątem beta).
 */
QPoint Robot::wyliczPunktKoncowy(double alpha, double beta){
//    qDebug("Metoda wyliczPunkt().");
    // liczymy współrzędne punktu złamania ramienia, później rysujemy to ramię
    QPoint posredni = wyliczProsta(QPoint(100, 150 - dlugoscRamienia), centralny, alpha);
//    rysujLinie(centralny, posredni);
//    rysujPunkt(posredni.x(), posredni.y());

    QPoint punktPomocniczy = wyliczProsta(QPoint(100, 150 - 2 * dlugoscRamienia), centralny, alpha);
//    rysujPunkt(punktPomocniczy.x(), punktPomocniczy.y());

    beta = -1 * M_PI + beta;

    QPoint punktKoncowy = wyliczProsta(punktPomocniczy, posredni, beta);
//    rysujPunkt(punktKoncowy.x(), punktKoncowy.y());
//    kolorujPunkt(punktKoncowy.x(), punktKoncowy.y());
//    rysujLinie(posredni, punktKoncowy);

    return punktKoncowy;
}

/**
 * 9 lutego 2019
 * Metoda pomocnicza.
 * Dostaje początek i koniec linii oraz
 */
QPoint Robot::wyliczProsta(QPoint poczatek, QPoint koniec, double kat){
    int xx = 0;
    int yy = 0;
    double radiany = kat * M_PI; //(M_PI / 180.0);
//    qDebug("Radiany: %f", radiany);
    double cosinus = cos(radiany);
    double sinus = sin(radiany);
    xx = (int) (cosinus * (poczatek.x() - koniec.x()) - sinus * (poczatek.y() - koniec.y()) + koniec.x());
    yy = (int) (sinus * (poczatek.x() - koniec.x()) + cosinus * (poczatek.y() - koniec.y()) + koniec.y());
//    qDebug("Początek: (%d, %d), Koniec: (%d, %d), wyliczony: (%d, %d)", poczatek.x(), poczatek.y(), koniec.x(), koniec.y(), xx, yy );
    return QPoint(xx, yy);
}

/**
 * 9 lutego 2019
 * Metoda odpowiada za narysowanie lini pomiędzy podanymi punktami.
 * Zastosowałem algorytm z LGiMów.
 *
 * W wersji drugiej źródłem jest:
 * https://stackoverflow.com/questions/5139028/qt-drawing-lines
 */
void Robot::rysujLinie(QPoint p1, QPoint p2){
//    qDebug("Rysujemy linię pomiędzy punktami (%d, %d) oraz (%d, %d)", p1.x(), p1.y(), p2.x(), p2.y());
    QPainter p(this->img1);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen(QPen(Qt::black, 2));
    p.drawLine(p1, p2);
    this->update();
}

/**
 * 9 lutego 2019
 * Metoda odpowiada za wyczyszczenie ekranu, czyli img1 malujemy na biało,
 * później ponownie kolorujemy punkt zaczepienia ramienia robota.
 */
void Robot::czyscEkran(){
    img1->fill(Qt::white);
    int xP = centralny.x();
    int yP = centralny.y();
    img1->setPixel(xP, yP, Qt::black);
    img1->setPixel(xP + 1, yP, Qt::black);
    img1->setPixel(xP - 1, yP, Qt::black);
    img1->setPixel(xP, yP + 1, Qt::black);
    img1->setPixel(xP, yP - 1, Qt::black);

    update();
}



/**
 * 28 stycznia 2019
 * Głowna metoda naszej sieci.
 * Uczymy, losując w każdej iteracji parę kontów, dla których wyliczamy, gdzie sięgnie ramię robota.
 * Następnie tak otrzymany przykład wrzucamy do sieci.
 *
 * Będziemy testować sieć w drugą stronę - wrzucamy parę współrzędnych punktu, a jako wynik otrzymujemy kąt, pod jakim
 * powinno być zgięte ramię robota.
 *
 * Zatem losujemy pewną ilość razy takie kąty.
 */
void Robot::uczSiec(){
    czyJestTrybNauki = false;
    qDebug("Rozpoczynamy uczenie sieci - test poprawności: warstwy: %d, neurony w warstwie: %d, neurony w wyjściowej: %d", warstwyUkryte.size(), warstwyUkryte[0].ilosc, warstwaWyj.size());

    for(int i = 0; i < 10; i++){
        QVector<double> wczesniejsze = QVector<double>();
        double alpha = losujKat();
        double beta = losujKat();
        qDebug("\n Alpha: %f, beta: %f", alpha, beta);
        QPoint local = wyliczPunktKoncowy(alpha, beta);
        kolorujPunkt(local.x(), local.y());
        wczesniejsze.push_back(local.x());
        wczesniejsze.push_back(local.y());
        // zatem nasz zestaw startowy do nauczenia w i-tym kroku pętli uczącej to (local, alpha, beta)

        // przebiegamy sieć w przód, liczymy sumę wejściową i wartość funkcji aktywacji na niej
        // następnie w każdej warstwie ukrytej podmieniamy zawartość tablicy "wcześniejsze",
        // aby mieć tam aktualne wcześniejsze wartośći (więc możemy mieć dużo neuronów w ukrytych)
        qDebug("Uczenie pętla główna");
        for(int j = 0; j < warstwyUkryte.size(); j++){
//            qDebug("Uczenie pętla po warstwach ukrytych: %d", j);
            Warstwa w = warstwyUkryte.at(j);
            for(int k = 0; k < w.ilosc; k++){
//                qDebug("Pojedyńcze neurony: %d", k);
                Neuron neuron = w.getWarstwa().at(k);
//                qDebug("Liczymy sumę");
                neuron.liczSumeWejsciowa(wczesniejsze);

                w.zamien(neuron, k);
            }
            warstwyUkryte[j] = w;
            wczesniejsze.clear();
            for(int k = 0; k < w.ilosc; k++){
//                qDebug("Cofanie - iteracja: %d", k);
                Neuron n = w.getWarstwa().at(k);
                double d = n.getWynik();
                wczesniejsze.push_back(d);
            }
        }
        // teraz warstwa końcowa
        for(int k = 0; k < warstwaWyj.size(); k++){
//            qDebug("Warstwa końcowa - iteracja: %d", k);
            Neuron n = warstwaWyj.at(k);
            n.liczSumeWejsciowa(wczesniejsze);
            warstwaWyj[k] = n;
        }
        wczesniejsze.clear();
        // przebiegamy sieć w tył
        qDebug("Przebiegamy sieć w tył");
        // warstwa końcowa
        for(int k = 0; k < warstwaWyj.size(); k++){
            Neuron n = warstwaWyj.at(k);
            double deltaP = 0;
            double zK  = n.getWynik();
            if(k == 0){
                // pierwsze wyjście to wartość alpha
                deltaP = (zK - alpha) * zK * (1 - zK);
            }
            if(k == 1){
                // drugie wyjście to wartość beta
                deltaP = (zK - beta) * zK * (1 - zK);
            }
            wczesniejsze.push_back(deltaP);
            warstwaWyj[k] = n;
        }
        qDebug("Teraz warstwy ukryte - iterujemy od końca:");
        for(int j = warstwyUkryte.size() - 1; j >= 0; j--){
            Warstwa w = warstwyUkryte.at(j);
            for(int k = 0; k < w.ilosc; k++){
                Neuron n = w.getWarstwa().at(k);
                double delta = 0;
                double sumaPom = 0;
                qDebug("zaraz liczymy sume do delty");
                for(int licznik = 1; licznik < n.getWejscia(); licznik ++){
                    double wKJ = n.getTablicaWag().at(licznik);
                    qDebug("wKJ: %f, licznik: %d", wKJ, licznik);
                    sumaPom = sumaPom + wczesniejsze[licznik-1] * wKJ  ;  //  <---- słaby punkt
                    qDebug("Koniec tej pętli");
                }
                delta = sumaPom * n.getWynik() * (1 - n.getWynik());
                n.setDelta(delta);
                w.getWarstwa()[k] = n;
            }
            qDebug("Czyscimy ponownie tablicę wag");
            // teraz wrzucamy, żeby zawsze w tabeli pomocniczej była odpowiednia zawartość
            wczesniejsze.clear();
            qDebug("uaktualniamy tablicę delt");
            for(int k = 0; k < w.ilosc; k++){
                double d = w.getWarstwa()[k].getDelta();
                wczesniejsze.push_back(d);
            }

        }
        wczesniejsze.clear();
        // zamieniamy wagi
        qDebug("Uaktualniamy wagi.");
        // warstwa wyjściowa
        int koniec = warstwyUkryte.size();
        for(int j = 0; j < warstwaWyj.size(); j++){
            Neuron n = warstwaWyj.at(j);
            n.uaktualnijWagi(warstwyUkryte.at(koniec - 1));
            warstwaWyj[j] = n;
        }
        // warstwy ukryte
        warstwyUkryte[0].uaktualnijWagi(local.x(), local.y());
        for(int licznikWarstw = 1; licznikWarstw < warstwyUkryte.size(); licznikWarstw++){
            warstwyUkryte[licznikWarstw].uaktualnijWagi(warstwyUkryte.at(licznikWarstw - 1));
        }

    }
    qDebug("Sieć została nauczona.");
}
