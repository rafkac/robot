#ifndef ROBOT_H
#define ROBOT_H

#include "neuron.h"
#include "warstwa.h"

#include <QMainWindow>
#include <QVector>
#include <QImage>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPushButton>

namespace Ui {
class Robot;
}

class Robot : public QMainWindow
{
    Q_OBJECT

public:
    explicit Robot(QWidget *parent = 0);
    ~Robot();


    double sigma(double s);
    double losujKat();

    void sprawdzamy();
    void kolorujPunkt(int x1, int y1);

    void rysujPunkt(int xP, int yP);

    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);

    QPoint wyliczPunktKoncowy(double alpha, double beta);
    QPoint wyliczProsta(QPoint poczatek, QPoint koniec, double kat);

    void rysujLinie(QPoint p1, QPoint p2);


private:
    Ui::Robot *ui;
    QVector<Neuron> warstwaWyj;
    QVector<Warstwa> warstwyUkryte;
    int iloscWarstwUkrytych;

    int dlugoscRamienia;
    int x;
    int y;

    QPoint centralny;

    bool czyJestTrybNauki;

    QImage *img1;
    QImage *img2;

public slots:
    void uczSiec();
    void czyscEkran();

};

#endif // ROBOT_H
