#include "warstwa.h"
#include <math.h>
#include "neuron.h"

/**
 * 28 stycznia 2019
 * RafKac
 * klasa odpowiada za pojedyńczą warstwę
 */

Warstwa::Warstwa(){
    ilosc = 1;
    warstwa = QVector<Neuron>();
}


/** 9 lutego 2019
 * W tym konstruktorze przyjmujemy dwa parametry.
 * Pierwszy z nich to ilość neuronów w warstwie.
 * Drugi to ilość wejść do każdego z nich (wszystkie mają tyle samo wejść).
 */
Warstwa::Warstwa(int i, int iloscWejsc){
    ilosc = i;
    warstwa = QVector<Neuron>();
    for(int j = 0; j < ilosc; j++){
        Neuron n = Neuron(iloscWejsc);
        warstwa.push_back(n);
    }
}

void Warstwa::setWarstwa(QVector<Neuron> warstwa){
    this->warstwa = warstwa;
}

QVector<Neuron> Warstwa::getWarstwa(){
    return warstwa;
}

void Warstwa::dodaj(Neuron n){
    warstwa.push_back(n);
}

Neuron Warstwa::zwroc(int i){
    return warstwa.at(i);
}

/**
 * 28 stycznia 2019
 * W które miejsce chcemy co wsadzić.
 * Konkretnie - wstaw dany neuron w miejsce podane jako drugi parametr wywołania.
 */
void Warstwa::zamien(Neuron n, int i){
    warstwa.replace(i, n);
}

/**
 * 4 lutego 2019
 * metoda dostaje jako argument warstwę wcześniejszą,
 * Odpowiada za przebieganie w przód.
 * Dla każdej jednostki liczy jej sumę wejściową oraz wyjście(wartość funkcji aktywacji na jednostce).
 *
 * Każdy neuron w warstwie k-tej ma tyle wejść, ile było neuronów w warstwie wcześniejszej + 1 (bias).
 *
 * Ponadto neuron ma tyle wag, ile wejść.
 * Dla każdego neuronu z danej warstwy od razu ustawiamy wartości sumy wejściowej oraz funkcji aktywacji na tej sumie.
 *
 * Konwencja: w_0 to waga do biasu, czyli obciążenia. Zatem rozmiar musi być + 1. WAŻNE!
 *
 * UWAGA:
 * W skutek działania tej metody pod zmienną wynik jest wartość sigma(suma).
 */
void Warstwa::operacjeNaWarstwie(Warstwa wczesniejsza){
    for(int i = 0; i < warstwa.size(); i++){
        Neuron roboczy = warstwa.at(i);
        double suma = roboczy.getTablicaWag().at(0);
        for(int j = 1; j < roboczy.getWejscia(); j++){
            suma += roboczy.getTablicaWag().at(j) * wczesniejsza.warstwa[j].getWynik();
        }
        roboczy.setSumeWejsciowa(suma);
        roboczy.setWynik(sigma(suma));
        warstwa[i] = roboczy;
    }
}

/**
 * 4 lutego 2019
 *
 * Ta metoda odpowiada za przebieganie sieci wstecz, a konkretnie za jeden krok takiego przebiegu.
 * Liczymy delta dla każdej jednostki:
 *  - dla wyjściowych:
 *          delta_k = (z_k - t_k) * fi'(b_k)                = (z_k - t_k) * z_k * (1 - z_k),
 *          delta_j = (suma_k=1^K delta_k*w_kj ) * fi'(a_j) = (suma ...) * y_j * (1 - y_j).
 */
void Warstwa::operacjeWstecz(Warstwa nastepna){
    for(int j = 0; j < warstwa.size(); j++){
        double s = 0;
        for(int k = 0; k < nastepna.warstwa.size(); k++){
            s += nastepna.warstwa[k].getDelta() * nastepna.warstwa[k].getTablicaWag().at(j);
        }
        double p = warstwa[j].getWynik();
        s = s * p * (1 - p);
        warstwa[j].setDelta(s);
    }
}

/**
 * 4 lutego 2019
 *
 * Metoda odpowiada za wykonanie operacji wstecz dla warstwy końcowej, dlatego nie może przyjąć jako parametru
 * następnej warstwy.
 */
void Warstwa::operacjeWsteczKoncowaWarstwa(double x, double y){
    double d = 0;
    for(int k = 0; k < warstwa.size(); k++){
        double suma = warstwa[k].getWynik();
        d = (suma - x) * suma * (1 - suma);
        warstwa[k].setDelta(d);
    }
}

/**
 * 14 lutego 2019
 * Metoda ma za zadanie uaktualnić wagi dla warstw wewnętrznych.
 * Jako parametr wywołania dostaje wcześniejszą warstwę.
 *
 * Dla każdego neuronu z naszej warstwy uaktualniamy jego wagi korzystając z wyjść warstw wcześniejszych.
 */
void Warstwa::uaktualnijWagi(Warstwa wczesniejsza){
    qDebug("uaktualniWagi(warstwa)");
    for(int j = 0; j < warstwa.size(); j++){
        Neuron n = warstwa.at(j);
        double waga = 0;
        // najpierw dla wagi zerowej, czyli tej od progu
        waga = n.getTablicaWag().at(0);
        waga += -1 * n.getStalaUczenia() * n.getDelta();
        n.zmienWage(0, waga);
        // pozostałe wagi
        waga = 0;
        for(int k = 1; k < n.getTablicaWag().size(); n++){
            waga = n.getTablicaWag().at(k);
            waga += -1 * n.getStalaUczenia() * n.getDelta() * wczesniejsza.getWarstwa().at(k).getWynik();
            n.zmienWage(k, waga);
        }

    }
    qDebug("uaktualnijWagi(warstwa) - koniec");
}

/** 14 lutego 2019
 * Metoda służy do uaktualnienia wag w pierwszej warstwie ukrytej.
 * Dlatego na wejściu dostaje tylko dwie liczby rzeczywiste, czyli
 * wartość x i y.
 * Pamiętajmy, że waga w0 odpowiada za wagę progu.
 */
void Warstwa::uaktualnijWagi(double x1, double x2){
    qDebug("UaktualnijWagi(double, double)");
    for(int numerNeuronu = 0; numerNeuronu < warstwa.size(); numerNeuronu++){
        Neuron n = warstwa.at(numerNeuronu);
        double waga = n.getTablicaWag().at(0);
        waga = waga - n.getStalaUczenia() * n.getDelta();
        n.zmienWage(0, waga);
        waga = 0;
        int numerWagi = 1;

        waga = n.getTablicaWag().at(numerWagi);
        waga = waga - n.getStalaUczenia() * n.getDelta() * x1;
        numerWagi = 2;
        waga = n.getTablicaWag().at(numerWagi);
        waga = waga - n.getStalaUczenia() * n.getDelta() * x2;

        warstwa[numerNeuronu] = n;
    }
    qDebug("UaktualnijWagi(double, double) - koniec");
}

/**
 * 4 lutego 2019
 *
 * Metoda liczy wartość funkcji simga na parametrze s
 */
double Warstwa::sigma(double s){
    return 1.0 / (1.0 + pow(M_E, -1*s));
}

