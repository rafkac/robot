#ifndef NEURON_H
#define NEURON_H

#include <QVector>
#include "warstwa.h"

class Neuron
{
public:
    Neuron();
    Neuron(int k);
    void setWejscia(int wejscia);
    int getWejscia();
    void setWynik(double wynik);
    double getWynik();
    void setTablicaWag(QVector<double> tablicaWag);
    QVector<double> getTablicaWag();
    void setStalaUczenia(double stalaUczenia);
    double getStalaUczenia();

    void setDelta(double delta);
    double getDelta();

    double zmienWage(int i, double wartosc);

    double getSumaWejsciowa();
    void setSumeWejsciowa(double sumaWejsciowa);

    void uaktualnijWagi(double w);
    void uaktualnijWagi(Warstwa wczesniejsza);
    void liczSumeWejsciowa(QVector<double> wejscie);

    double sigma(double s);

private:
    int wejscia;
    double wynik;
    double sumaWejsciowa;
    QVector<double> tablicaWag;
    double stalaUczenia;
    double delta;
    double prog;
};

#endif // NEURON_H
